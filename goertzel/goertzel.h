/*
 * goertzel.h
 *
 * Created: 2016-05-25 7:52:43 PM
 *  Author: Justin
 */ 


#ifndef GOERTZEL_H_
#define GOERTZEL_H_


#define SAMPLING_RATE		28846.15	// Hz
#define SAMPLE_SIZE			256

#define COEFF_PRECISION		13
#define	ADC_REDUCE			0
#define MAG_REDUCE			4

#define PI					3.14159


#include <stdio.h>
#include <avr/io.h>
#include <math.h>
#include "../ir_detectors/ir_detectors.h"


static volatile float coeffSingle;
static volatile float Q1;
static volatile float Q2;
static volatile float cosine;


/* Call this once, to precompute the constants. */
void initGoertzel(uint16_t *channelFrequencies, float *coeff);



void InitGoertzelAnotherFreq(float frequency);

/* Optimized Goertzel */
/* Call this after every block to get the RELATIVE magnitude squared. */
float getMagnitudeSquared(float curCoeff);

void InitAndTest(float frequency, uint16_t *samplesADC);


#endif /* GOERTZEL_H_ */

