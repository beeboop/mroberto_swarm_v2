/*
 * goertzel.c
 *
 * Created: 2016-05-25 7:52:55 PM
 *  Author: Justin
 */ 

#include "goertzel.h"


void initGoertzel(uint16_t *channelFrequencies, float *coeff)
{
	volatile int		k;
	volatile float		omega;

	// go through all the channels
	for (short curChan = 0; curChan < NUM_OF_CHANNELS; curChan++) {
		k = (int)(0.5 + ((((float)SAMPLE_SIZE) * channelFrequencies[curChan]) / SAMPLING_RATE));
		omega = (2.0 * PI * k) / ((float)SAMPLE_SIZE);
		cosine = cos(omega);
		coeff[curChan] = 2.0 * cosine;
	}
}


void InitGoertzelAnotherFreq(float frequency)
{
	int		k;
	float	omega;

	k = (int)(0.5 + ((((float)SAMPLE_SIZE) * frequency) / SAMPLING_RATE));
	omega = (2.0 * PI * k) / ((float)SAMPLE_SIZE);
	cosine = cos(omega);
	coeffSingle = 2.0 * cosine;
}


float getMagnitudeSquared(float curCoeff)
{
	volatile float result = Q1 * Q1 + Q2 * Q2 - Q1 * Q2 * curCoeff;
	return result;
}


void InitAndTest(float frequency, uint16_t *samplesADC)
{
	uint32_t magnitudeSquared;
	int32_t multip;
	int16_t coeffTest;
	int32_t Q0 = 0, Q1 = 0, Q2 = 0;
	
	printf("Freq=%d   ", (int16_t)frequency);
	InitGoertzelAnotherFreq(frequency);

	coeffTest = (int16_t)(coeffSingle * (1 << COEFF_PRECISION));

	//--- process the samples ---/
	for (uint16_t index = 0; index < SAMPLE_SIZE; index++) {
		multip = coeffTest * Q1;
		Q0 = (multip >> COEFF_PRECISION) - Q2 + (samplesADC[index] >> ADC_REDUCE);
		Q2 = Q1;
		Q1 = Q0;
		
		//printf("%u, ", Q0);
	}
	//--- do the 'optimized' Goertzel processing ---/
	// divide the answer by 2^6
	multip = coeffTest * Q1;
	
	magnitudeSquared = (Q1 * Q1 + Q2 * (Q2 - (multip >> COEFF_PRECISION))) >> MAG_REDUCE;

	// reset Goertzel state variables
	Q1 = 0;
	Q2 = 0;

	printf("rel mag=%u\r\n", magnitudeSquared);
	
	return;
}
