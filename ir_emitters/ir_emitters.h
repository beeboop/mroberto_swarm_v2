/*******************************************************************************************
*
* ir_emitters.h				Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/


#ifndef IR_EMITTERS_H_
#define IR_EMITTERS_H_

#define PRE_SCALAR_PWM	8

#define	turnOFF_PWM()	TCCR1B &= ~(1 << CS11)	// disables clock source to PWM timer
#define	turnON_PWM()	TCCR1B |= (1 << CS11)	// enables clock source to timer


#include <avr/io.h>
#include <stdlib.h>


void init_IREmitters(void);
void set_IRFrequency(unsigned short frequencyPWM);



#endif /* IR_EMITTERS_H_ */