/*******************************************************************************************
*
* ir_emitters.c				Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/

#include "ir_emitters.h"


void init_IREmitters(void)
{
	// change the PWM emitter pin to output
	DDRB |= (1<<DDB1);
	// disable PWM output for now
	PORTB &= ~(1<<DDB1);
	
	/* setup our PWM registers */
	TCCR1A = (1 << COM1A1) | (1 << COM1B1);		// phase and frequency correct mode. NON-inverted mode
	TCCR1B = (1 << WGM13) | (1 << CS11);		// select mode 8 and select divide by 8 on main clock
	
	return;
}


void set_IRFrequency(unsigned short frequencyPWM)
{
	// PWM freq = (sys_clk) / (2 * PRE_SCALAR * ICR1)
	// PRE_SCALAR = 8
	// sys_clk = 12 MHz
	// ICR1 = sys_clk / (2 * PRE_SCALAR * PWM_freq)
	//ICR1 = 1250;	// set it to 400Hz
	ICR1 = (int)(((float)F_CPU) / (2.0 * ((float)PRE_SCALAR_PWM) * ((float)frequencyPWM)));		// set it to approx. 150 KHz
	// PWM duty = ICR1 * DUTY_PERCENT
	// i.e., for 50% duty cycle, PWM_DUTY = ICR1 * 0.50
	// assign values to OCR Registers, which output the PWM duty cycle
	OCR1A = (int)(ICR1 / 2.0);
	OCR1B = (int)(ICR1 / 2.0);
	
	return;
}



