/*******************************************************************************************
*
* ir_translators.c			Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/

#include "ir_translator.h"


void translateRawIR(uint16_t rawIR[NUM_OF_IRSENS][NUM_OF_CHANNELS],
	float scaleIR[NUM_OF_IRSENS][NUM_OF_CHANNELS])
{
	// scale factor for all channels since amplitude decreases as frequency increases
	float scaleChan[NUM_OF_CHANNELS] = {1.0, SCALE_CHANNEL_1, SCALE_CHANNEL_2,
		SCALE_CHANNEL_3, SCALE_CHANNEL_4};
		
	float maxRawIR_50mm[NUM_OF_IRSENS] = {MAX_RAW_IR_1, MAX_RAW_IR_2, MAX_RAW_IR_3,
		MAX_RAW_IR_4, MAX_RAW_IR_5, MAX_RAW_IR_6};
	
	// normalize the raw IR readings with the MAX RAW IR values at 50 mm for each detector
	for (uint8_t i = 0; i < NUM_OF_IRSENS; i++) {
		for (uint8_t j = 0; j < NUM_OF_CHANNELS; j++) {
			scaleIR[i][j] = (((float)rawIR[i][j]) * scaleChan[j]) / maxRawIR_50mm[i];
		}
	}
}
