/*******************************************************************************************
*
* ir_translators.h			Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/


#ifndef IR_TRANSLATOR_H_
#define IR_TRANSLATOR_H_


#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <math.h>
#include "ir_detectors.h"




void translateRawIR(uint16_t rawIR[NUM_OF_IRSENS][NUM_OF_CHANNELS],
	float scaleIR[NUM_OF_IRSENS][NUM_OF_CHANNELS]);


#endif /* IR_TRANSLATOR_H_ */

