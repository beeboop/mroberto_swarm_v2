/*******************************************************************************************
*
* ir_detectors.h			Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/


/*
*	Orientation of the IR phototransistors:
*
*		 (FRONT OF ROBOT)
*
*		   FL	FF   FR
*			\	|	/
*			/	|	\
*		   BL   BB   BR
*
*		  (BACK OF ROBOT)
*
*	ADC0 -> BR (back right)
*	ADC1 -> FL (front left)
*	ADC2 -> FF (front front)
*	ADC3 -> FR (front right)
*	ADC4 -> *NOT USED (designated as SDA)
*	ADC5 -> *NOT USED (designated as SCL)
*	ADC6 -> BB (back back)
*	ADC7 -> BL (back left)
*
*/

#ifndef IR_DETECTORS_H_
#define IR_DETECTORS_H_

// number of channels we're allowing
#define NUM_OF_CHANNELS	5
// we have 6 IR detectors
#define NUM_OF_IRSENS	6

// channels (frequencies) for PWM
#define CHANNEL_0_PWM	4925	// Hz
#define CHANNEL_1_PWM	5750	// Hz
#define CHANNEL_2_PWM	6610	// Hz
#define CHANNEL_3_PWM	7840	// Hz
#define CHANNEL_4_PWM	8650	// Hz

// channels (frequencies) for reading ADCs
#define CHANNEL_0_ADC	5300	// Hz
#define CHANNEL_1_ADC	6200	// Hz
#define CHANNEL_2_ADC	7100	// Hz
#define CHANNEL_3_ADC	8450	// Hz
#define CHANNEL_4_ADC	9350	// Hz


#define PI	3.14159

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <math.h>
#include "../goertzel/goertzel.h"


/********************************************************************************************
*	CHOOSE THE CORRECT HEADER FILE FOR CORRECT CALIBRATION DATA!
********************************************************************************************/
//#include "mod_1_profile.h"
//#include "mod_2_profile.h"
//#include "mod_3_profile.h"
//#include "mod_4_profile.h"
//#include "mod_5_profile.h"
#include "mod_6_profile.h"
//#include "mod_7_profile.h"
//#include "mod_8_profile.h"
//#include "mod_9_profile.h"


volatile uint16_t	proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS];
volatile uint16_t	bearingsAllChan[NUM_OF_CHANNELS];
volatile uint8_t	proximitiesAllChan[NUM_OF_CHANNELS];



void init_ADC(void);
void getAllIR(uint16_t *fx, uint8_t channelADC);
void getBearings(float proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS]);
void getProximities(float proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS], uint16_t bearingsAllChan[NUM_OF_CHANNELS]);


#endif /* IR_DETECTORS_H_ */

