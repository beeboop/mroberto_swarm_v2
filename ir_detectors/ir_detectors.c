/*******************************************************************************************
*
* ir_detectors.c			Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/

#include "ir_detectors.h"

// below variables hold the proximities and relative angles of all channels
volatile uint8_t proximitiesAllChan[NUM_OF_CHANNELS];
volatile uint16_t bearingsAllChan[NUM_OF_CHANNELS];


void init_ADC(void)
{
	// select Vref = AVcc
	ADMUX |= (1<<REFS0);

	// set prescaler to 16 and enable ADC
	// prescaler is set such that we achieve approximately 28.8 Ksps by the ADC
	// we have 12 MHz (overclocked) as our system clock and we know it takes 13 ADC clock 
	// cycles to get one reading
	// this means that we can do 12 MHz / 32 prescalar / 13 ADC clock cycle = 28846.15 sps
	// this also means we have 28846.15 sps / 256 sampels = 112.68 Hz/bin
	ADCSRA |= (1 << ADPS2)|(0 << ADPS1)|(1 << ADPS0) | (1 << ADEN);
	
	//single conversion mode
	ADCSRA |= (1<<ADSC);

	return;	
}


void getAllIR(uint16_t *fx, uint8_t channelADC)
{
	// clear ADC channel mux register
	ADMUX &= 0xF0;
	// select which ADC channel to read from
	ADMUX |= channelADC;
	// get up to SAMPLE_SIZE number of samples
	for (short i = 0; i < SAMPLE_SIZE; i++) {
		// do single conversion
		ADCSRA |= (1<<ADSC);
		while(ADCSRA & (1<<ADSC));
		fx[i] = (uint16_t)ADCW;
	}
}


void getBearings(float proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS])
{
	double sinValue = 0.0;
	double cosValue = 0.0;
	double tempRadAngle = 0.0;
	float sensorIRPositions[NUM_OF_IRSENS] = {0.0, -1.0472, -2.0944, 3.14159, 2.0944, 1.0472};
	
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		for (short j = 0; j < NUM_OF_IRSENS; j++) {
			sinValue += proximitiesIR[j][i] * sin(sensorIRPositions[j]);
			cosValue += proximitiesIR[j][i] * cos(sensorIRPositions[j]);
		}
		// arctangent our sin and cos values
		tempRadAngle = atan2(sinValue, cosValue);
		// convert from radians to degrees
		bearingsAllChan[i] = (uint16_t)((tempRadAngle * (180.0 / PI)) + 360.0);
		if (bearingsAllChan[i] > 360) {
			// convert all degrees to positive values
			bearingsAllChan[i] -= 360;
		}
		// reset our cosine and sine sums
		sinValue = 0.0;
		cosValue = 0.0;
	}
	
	return;
}

/*
void getProximities(uint8_t proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS], uint16_t bearingsAllChan[NUM_OF_CHANNELS])
{*/
void getProximities(float proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS], uint16_t bearingsAllChan[NUM_OF_CHANNELS])
{
	// add all the scaled IR readings together for each channel
	float addedIRReadings[NUM_OF_CHANNELS];
	// initialize everything to 0
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		addedIRReadings[i] = 0.0;
	}
	// add up all the IR readings for each channel
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		for (short j = 0; j < NUM_OF_IRSENS; j++) {
			addedIRReadings[i] += proximitiesIR[j][i];
		}
	}	
	
	// get proximities from channels 0 to NUM_OF_CHANNELS
	for (uint8_t curChan = 0; curChan < NUM_OF_CHANNELS; curChan++) {
		
		float LIM_50MM;
		float LIM_75MM;
		float LIM_100MM;
		float LIM_125MM;
		
		// between 0 to 15 degrees
		if ((bearingsAllChan[curChan] >= 0) && (bearingsAllChan[curChan] <= 15)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_0_15 * bearingsAllChan[curChan] + INT_50MM_0_15;
			LIM_75MM = SLO_75MM_0_15 * bearingsAllChan[curChan] + INT_75MM_0_15;
			LIM_100MM = SLO_100MM_0_15 * bearingsAllChan[curChan] + INT_100MM_0_15;
			LIM_125MM = SLO_125MM_0_15 * bearingsAllChan[curChan] + INT_125MM_0_15;
		}
		// between 15 to 30 degrees
		else if ((bearingsAllChan[curChan] >= 15) && (bearingsAllChan[curChan] <= 30)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_15_30 * bearingsAllChan[curChan] + INT_50MM_15_30;
			LIM_75MM = SLO_75MM_15_30 * bearingsAllChan[curChan] + INT_75MM_15_30;
			LIM_100MM = SLO_100MM_15_30 * bearingsAllChan[curChan] + INT_100MM_15_30;
			LIM_125MM = SLO_125MM_15_30 * bearingsAllChan[curChan] + INT_125MM_15_30;
		}
		// between 30 to 45 degrees
		else if ((bearingsAllChan[curChan] >= 30) && (bearingsAllChan[curChan] <= 45)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_30_45 * bearingsAllChan[curChan] + INT_50MM_30_45;
			LIM_75MM = SLO_75MM_30_45 * bearingsAllChan[curChan] + INT_75MM_30_45;
			LIM_100MM = SLO_100MM_30_45 * bearingsAllChan[curChan] + INT_100MM_30_45;
			LIM_125MM = SLO_125MM_30_45 * bearingsAllChan[curChan] + INT_125MM_30_45;
		}
		// between 45 to 60 degrees
		else if ((bearingsAllChan[curChan] >= 45) && (bearingsAllChan[curChan] <= 60)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_45_60 * bearingsAllChan[curChan] + INT_50MM_45_60;
			LIM_75MM = SLO_75MM_45_60 * bearingsAllChan[curChan] + INT_75MM_45_60;
			LIM_100MM = SLO_100MM_45_60 * bearingsAllChan[curChan] + INT_100MM_45_60;
			LIM_125MM = SLO_125MM_45_60 * bearingsAllChan[curChan] + INT_125MM_45_60;
		}
		// between 60 to 75 degrees
		else if ((bearingsAllChan[curChan] >= 60) && (bearingsAllChan[curChan] <= 75)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_60_75 * bearingsAllChan[curChan] + INT_50MM_60_75;
			LIM_75MM = SLO_75MM_60_75 * bearingsAllChan[curChan] + INT_75MM_60_75;
			LIM_100MM = SLO_100MM_60_75 * bearingsAllChan[curChan] + INT_100MM_60_75;
			LIM_125MM = SLO_125MM_60_75 * bearingsAllChan[curChan] + INT_125MM_60_75;
		}
		// between 75 to 90 degrees
		else if ((bearingsAllChan[curChan] >= 75) && (bearingsAllChan[curChan] <= 90)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_75_90 * bearingsAllChan[curChan] + INT_50MM_75_90;
			LIM_75MM = SLO_75MM_75_90 * bearingsAllChan[curChan] + INT_75MM_75_90;
			LIM_100MM = SLO_100MM_75_90 * bearingsAllChan[curChan] + INT_100MM_75_90;
			LIM_125MM = SLO_125MM_75_90 * bearingsAllChan[curChan] + INT_125MM_75_90;
		}
		// between 90 to 105 degrees
		else if ((bearingsAllChan[curChan] >= 90) && (bearingsAllChan[curChan] <= 105)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_90_105 * bearingsAllChan[curChan] + INT_50MM_90_105;
			LIM_75MM = SLO_75MM_90_105 * bearingsAllChan[curChan] + INT_75MM_90_105;
			LIM_100MM = SLO_100MM_90_105 * bearingsAllChan[curChan] + INT_100MM_90_105;
			LIM_125MM = SLO_125MM_90_105 * bearingsAllChan[curChan] + INT_125MM_90_105;
		}
		// between 105 to 120 degrees
		else if ((bearingsAllChan[curChan] >= 105) && (bearingsAllChan[curChan] <= 120)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_105_120 * bearingsAllChan[curChan] + INT_50MM_105_120;
			LIM_75MM = SLO_75MM_105_120 * bearingsAllChan[curChan] + INT_75MM_105_120;
			LIM_100MM = SLO_100MM_105_120 * bearingsAllChan[curChan] + INT_100MM_105_120;
			LIM_125MM = SLO_125MM_105_120 * bearingsAllChan[curChan] + INT_125MM_105_120;
		}
		// between 120 to 135 degrees
		else if ((bearingsAllChan[curChan] >= 120) && (bearingsAllChan[curChan] <= 135)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_120_135 * bearingsAllChan[curChan] + INT_50MM_120_135;
			LIM_75MM = SLO_75MM_120_135 * bearingsAllChan[curChan] + INT_75MM_120_135;
			LIM_100MM = SLO_100MM_120_135 * bearingsAllChan[curChan] + INT_100MM_120_135;
			LIM_125MM = SLO_125MM_120_135 * bearingsAllChan[curChan] + INT_125MM_120_135;
		}
		// between 135 to 150 degrees
		else if ((bearingsAllChan[curChan] >= 135) && (bearingsAllChan[curChan] <= 150)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_135_150 * bearingsAllChan[curChan] + INT_50MM_135_150;
			LIM_75MM = SLO_75MM_135_150 * bearingsAllChan[curChan] + INT_75MM_135_150;
			LIM_100MM = SLO_100MM_135_150 * bearingsAllChan[curChan] + INT_100MM_135_150;
			LIM_125MM = SLO_125MM_135_150 * bearingsAllChan[curChan] + INT_125MM_135_150;
		}
		// between 150 to 165 degrees
		else if ((bearingsAllChan[curChan] >= 150) && (bearingsAllChan[curChan] <= 165)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_150_165 * bearingsAllChan[curChan] + INT_50MM_150_165;
			LIM_75MM = SLO_75MM_150_165 * bearingsAllChan[curChan] + INT_75MM_150_165;
			LIM_100MM = SLO_100MM_150_165 * bearingsAllChan[curChan] + INT_100MM_150_165;
			LIM_125MM = SLO_125MM_150_165 * bearingsAllChan[curChan] + INT_125MM_150_165;
		}
		// between 165 to 180 degrees
		else if ((bearingsAllChan[curChan] >= 165) && (bearingsAllChan[curChan] <= 180)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_165_180 * bearingsAllChan[curChan] + INT_50MM_165_180;
			LIM_75MM = SLO_75MM_165_180 * bearingsAllChan[curChan] + INT_75MM_165_180;
			LIM_100MM = SLO_100MM_165_180 * bearingsAllChan[curChan] + INT_100MM_165_180;
			LIM_125MM = SLO_125MM_165_180 * bearingsAllChan[curChan] + INT_125MM_165_180;
		}
		// between 180 to 195 degrees
		else if ((bearingsAllChan[curChan] >= 180) && (bearingsAllChan[curChan] <= 195)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_180_195 * bearingsAllChan[curChan] + INT_50MM_180_195;
			LIM_75MM = SLO_75MM_180_195 * bearingsAllChan[curChan] + INT_75MM_180_195;
			LIM_100MM = SLO_100MM_180_195 * bearingsAllChan[curChan] + INT_100MM_180_195;
			LIM_125MM = SLO_125MM_180_195 * bearingsAllChan[curChan] + INT_125MM_180_195;
		}
		// between 195 to 210 degrees
		else if ((bearingsAllChan[curChan] >= 195) && (bearingsAllChan[curChan] <= 210)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_195_210 * bearingsAllChan[curChan] + INT_50MM_195_210;
			LIM_75MM = SLO_75MM_195_210 * bearingsAllChan[curChan] + INT_75MM_195_210;
			LIM_100MM = SLO_100MM_195_210 * bearingsAllChan[curChan] + INT_100MM_195_210;
			LIM_125MM = SLO_125MM_195_210 * bearingsAllChan[curChan] + INT_125MM_195_210;
		}
		// between 210 to 225 degrees
		else if ((bearingsAllChan[curChan] >= 210) && (bearingsAllChan[curChan] <= 225)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_210_225 * bearingsAllChan[curChan] + INT_50MM_210_225;
			LIM_75MM = SLO_75MM_210_225 * bearingsAllChan[curChan] + INT_75MM_210_225;
			LIM_100MM = SLO_100MM_210_225 * bearingsAllChan[curChan] + INT_100MM_210_225;
			LIM_125MM = SLO_125MM_210_225 * bearingsAllChan[curChan] + INT_125MM_210_225;
		}
		// between 225 to 240 degrees
		else if ((bearingsAllChan[curChan] >= 225) && (bearingsAllChan[curChan] <= 240)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_225_240 * bearingsAllChan[curChan] + INT_50MM_225_240;
			LIM_75MM = SLO_75MM_225_240 * bearingsAllChan[curChan] + INT_75MM_225_240;
			LIM_100MM = SLO_100MM_225_240 * bearingsAllChan[curChan] + INT_100MM_225_240;
			LIM_125MM = SLO_125MM_225_240 * bearingsAllChan[curChan] + INT_125MM_225_240;
		}
		// between 240 to 255 degrees
		else if ((bearingsAllChan[curChan] >= 240) && (bearingsAllChan[curChan] <= 255)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_240_255 * bearingsAllChan[curChan] + INT_50MM_240_255;
			LIM_75MM = SLO_75MM_240_255 * bearingsAllChan[curChan] + INT_75MM_240_255;
			LIM_100MM = SLO_100MM_240_255 * bearingsAllChan[curChan] + INT_100MM_240_255;
			LIM_125MM = SLO_125MM_240_255 * bearingsAllChan[curChan] + INT_125MM_240_255;
		}
		// between 255 to 270 degrees
		else if ((bearingsAllChan[curChan] >= 255) && (bearingsAllChan[curChan] <= 270)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_255_270 * bearingsAllChan[curChan] + INT_50MM_255_270;
			LIM_75MM = SLO_75MM_255_270 * bearingsAllChan[curChan] + INT_75MM_255_270;
			LIM_100MM = SLO_100MM_255_270 * bearingsAllChan[curChan] + INT_100MM_255_270;
			LIM_125MM = SLO_125MM_255_270 * bearingsAllChan[curChan] + INT_125MM_255_270;
		}
		// between 270 to 285 degrees
		else if ((bearingsAllChan[curChan] >= 270) && (bearingsAllChan[curChan] <= 285)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_270_285 * bearingsAllChan[curChan] + INT_50MM_270_285;
			LIM_75MM = SLO_75MM_270_285 * bearingsAllChan[curChan] + INT_75MM_270_285;
			LIM_100MM = SLO_100MM_270_285 * bearingsAllChan[curChan] + INT_100MM_270_285;
			LIM_125MM = SLO_125MM_270_285 * bearingsAllChan[curChan] + INT_125MM_270_285;
		}
		// between 285 to 300 degrees
		else if ((bearingsAllChan[curChan] >= 285) && (bearingsAllChan[curChan] <= 300)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_285_300 * bearingsAllChan[curChan] + INT_50MM_285_300;
			LIM_75MM = SLO_75MM_285_300 * bearingsAllChan[curChan] + INT_75MM_285_300;
			LIM_100MM = SLO_100MM_285_300 * bearingsAllChan[curChan] + INT_100MM_285_300;
			LIM_125MM = SLO_125MM_285_300 * bearingsAllChan[curChan] + INT_125MM_285_300;
		}
		// between 300 to 315 degrees
		else if ((bearingsAllChan[curChan] >= 300) && (bearingsAllChan[curChan] <= 315)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_300_315 * bearingsAllChan[curChan] + INT_50MM_300_315;
			LIM_75MM = SLO_75MM_300_315 * bearingsAllChan[curChan] + INT_75MM_300_315;
			LIM_100MM = SLO_100MM_300_315 * bearingsAllChan[curChan] + INT_100MM_300_315;
			LIM_125MM = SLO_125MM_300_315 * bearingsAllChan[curChan] + INT_125MM_300_315;
		}
		// between 315 to 330 degrees
		else if ((bearingsAllChan[curChan] >= 315) && (bearingsAllChan[curChan] <= 330)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_315_330 * bearingsAllChan[curChan] + INT_50MM_315_330;
			LIM_75MM = SLO_75MM_315_330 * bearingsAllChan[curChan] + INT_75MM_315_330;
			LIM_100MM = SLO_100MM_315_330 * bearingsAllChan[curChan] + INT_100MM_315_330;
			LIM_125MM = SLO_125MM_315_330 * bearingsAllChan[curChan] + INT_125MM_315_330;
		}
		// between 330 to 345 degrees
		else if ((bearingsAllChan[curChan] >= 330) && (bearingsAllChan[curChan] <= 345)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_330_345 * bearingsAllChan[curChan] + INT_50MM_330_345;
			LIM_75MM = SLO_75MM_330_345 * bearingsAllChan[curChan] + INT_75MM_330_345;
			LIM_100MM = SLO_100MM_330_345 * bearingsAllChan[curChan] + INT_100MM_330_345;
			LIM_125MM = SLO_125MM_330_345 * bearingsAllChan[curChan] + INT_125MM_330_345;
		}
		// between 345 to 360 degrees
		else if ((bearingsAllChan[curChan] >= 345) && (bearingsAllChan[curChan] <= 360)) {
			// get the limits of 50, 75, and 100 mm distances
			LIM_50MM = SLO_50MM_345_360 * bearingsAllChan[curChan] + INT_50MM_345_360;
			LIM_75MM = SLO_75MM_345_360 * bearingsAllChan[curChan] + INT_75MM_345_360;
			LIM_100MM = SLO_100MM_345_360 * bearingsAllChan[curChan] + INT_100MM_345_360;
			LIM_125MM = SLO_125MM_345_360 * bearingsAllChan[curChan] + INT_125MM_345_360;
		}
		
		//-- check where the summed IR readings lie between and approximate the distance
		// it's less than 50 mm
		float m = 0;
		float b = 0;
		if (addedIRReadings[curChan] >= LIM_50MM) {//1.6873
			m = (50.0 - 0.0)/(LIM_50MM - 5.0);
			b = 50.0 - m * LIM_50MM;
		}
		// it's between 50 to 75 mm
		else if ((addedIRReadings[curChan] <= LIM_50MM) && (addedIRReadings[curChan] >= LIM_75MM)) {
			m = (75.0 - 50.0)/(LIM_75MM - LIM_50MM);
			b = 75.0 - m * LIM_75MM;
		}
		// it's between 75 to 100 mm
		else if ((addedIRReadings[curChan] <= LIM_75MM) && (addedIRReadings[curChan] >= LIM_100MM)) {
			m = (100.0 - 75.0)/(LIM_100MM - LIM_75MM);
			b = 100.0 - m * LIM_100MM;
		}
		// it's between 100 to 125 mm, or greater
		else if ((addedIRReadings[curChan] <= LIM_100MM)) {//&& (addedIRReadings[curChan] >= LIM_100MM) {
			m = (125.0 - 100.0)/(LIM_125MM - LIM_100MM);
			b = 100.0 - m * LIM_100MM;
		}
		// calculate our final relative proximity of the channel
		float tempAnswer = (m * addedIRReadings[curChan] + b);
		if (tempAnswer < 0.0) {
			tempAnswer = 0.0;
		}
		proximitiesAllChan[curChan] = (uint8_t)tempAnswer;
	}
}




