/*******************************************************************************************
*
* ir_cubic_parameters_mod_1.h		Copyright (C) 2016 Justin Y. Kim
*									last edit: April 22, 2016
*
*******************************************************************************************/


#ifndef IR_CUBIC_PARAMETERS_MOD_1_H_
#define IR_CUBIC_PARAMETERS_MOD_1_H_


// maximum raw IR (ADC) reading possible when emitter is right beside detector
//	NOTE: set to a value less than uint8_t sized integer
#define MAX_IR_INTENSITY	254.0
// lowest amount of raw IR reading allowed to be considered non-noise
#define NOISE_ALLOWED		0
// the maximum distance the IR sensor will read up to
#define MAX_DISTANCE		150		// in mm units

/************** CHANNEL 0 **************/
// IR detector 1
#define CHAN0_IR_1_50_MM	0.0
#define CHAN0_IR_1_100_MM	0.0
#define CHAN0_IR_1_150_MM	0.0
#define CHAN0_IR_1_A_50		0.0
#define CHAN0_IR_1_B_50		0.0
#define CHAN0_IR_1_C_50		0.0
#define CHAN0_IR_1_A_100	0.0
#define CHAN0_IR_1_B_100	0.0
#define CHAN0_IR_1_C_100	0.0
#define CHAN0_IR_1_A_150	0.0
#define CHAN0_IR_1_B_150	0.0
#define CHAN0_IR_1_C_150	0.0
// IR detector 2
#define CHAN0_IR_2_50_MM	0.0
#define CHAN0_IR_2_100_MM	0.0
#define CHAN0_IR_2_150_MM	0.0
#define CHAN0_IR_2_A_50		0.0
#define CHAN0_IR_2_B_50		0.0
#define CHAN0_IR_2_C_50		0.0
#define CHAN0_IR_2_A_100	0.0
#define CHAN0_IR_2_B_100	0.0
#define CHAN0_IR_2_C_100	0.0
#define CHAN0_IR_2_A_150	0.0
#define CHAN0_IR_2_B_150	0.0
#define CHAN0_IR_2_C_150	0.0
// IR detector 3
#define CHAN0_IR_3_50_MM	0.0
#define CHAN0_IR_3_100_MM	0.0
#define CHAN0_IR_3_150_MM	0.0
#define CHAN0_IR_3_A_50		0.0
#define CHAN0_IR_3_B_50		0.0
#define CHAN0_IR_3_C_50		0.0
#define CHAN0_IR_3_A_100	0.0
#define CHAN0_IR_3_B_100	0.0
#define CHAN0_IR_3_C_100	0.0
#define CHAN0_IR_3_A_150	0.0
#define CHAN0_IR_3_B_150	0.0
#define CHAN0_IR_3_C_150	0.0
// IR detector 4
#define CHAN0_IR_4_50_MM	0.0
#define CHAN0_IR_4_100_MM	0.0
#define CHAN0_IR_4_150_MM	0.0
#define CHAN0_IR_4_A_50		0.0
#define CHAN0_IR_4_B_50		0.0
#define CHAN0_IR_4_C_50		0.0
#define CHAN0_IR_4_A_100	0.0
#define CHAN0_IR_4_B_100	0.0
#define CHAN0_IR_4_C_100	0.0
#define CHAN0_IR_4_A_150	0.0
#define CHAN0_IR_4_B_150	0.0
#define CHAN0_IR_4_C_150	0.0
// IR detector 5
#define CHAN0_IR_5_50_MM	0.0
#define CHAN0_IR_5_100_MM	0.0
#define CHAN0_IR_5_150_MM	0.0
#define CHAN0_IR_5_A_50		0.0
#define CHAN0_IR_5_B_50		0.0
#define CHAN0_IR_5_C_50		0.0
#define CHAN0_IR_5_A_100	0.0
#define CHAN0_IR_5_B_100	0.0
#define CHAN0_IR_5_C_100	0.0
#define CHAN0_IR_5_A_150	0.0
#define CHAN0_IR_5_B_150	0.0
#define CHAN0_IR_5_C_150	0.0
// IR detector 6
#define CHAN0_IR_6_50_MM	0.0
#define CHAN0_IR_6_100_MM	0.0
#define CHAN0_IR_6_150_MM	0.0
#define CHAN0_IR_6_A_50		0.0
#define CHAN0_IR_6_B_50		0.0
#define CHAN0_IR_6_C_50		0.0
#define CHAN0_IR_6_A_100	0.0
#define CHAN0_IR_6_B_100	0.0
#define CHAN0_IR_6_C_100	0.0
#define CHAN0_IR_6_A_150	0.0
#define CHAN0_IR_6_B_150	0.0
#define CHAN0_IR_6_C_150	0.0


/*** CHANNELS 1 TO 8 SCALES ***/
#define SCALE_CHANNEL_1		0.0
#define SCALE_CHANNEL_2		0.0
#define SCALE_CHANNEL_3		0.0
#define SCALE_CHANNEL_4		0.0
#define SCALE_CHANNEL_5		0.0
#define SCALE_CHANNEL_6		0.0
#define SCALE_CHANNEL_7		0.0
#define SCALE_CHANNEL_8		0.0


#endif /* IR_CUBIC_PARAMETERS_MOD_1_H_ */

