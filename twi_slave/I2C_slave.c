/*******************************************************************************************
*
* I2C_slave.c				Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/

#include "I2C_slave.h"

// different frequencies for different channels
uint16_t channelFreq[NUM_OF_CHANNELS] = {CHANNEL_0_PWM, CHANNEL_1_PWM, CHANNEL_2_PWM,
	CHANNEL_3_PWM, CHANNEL_4_PWM};


void I2C_init(uint8_t address){
	// load address into TWI address register
	TWAR = address;
	// set the TWCR to enable address matching and enable TWI, clear TWINT, enable TWI interrupt
	TWCR = (1<<TWIE) | (1<<TWEA) | (1<<TWINT) | (1<<TWEN);
	// reset our addr counter
	bufferAddr_Proximities = 0;
	curIRDetector = 0;
	bufferAddr_Bearings = 0;
}

void I2C_stop(void){
	// clear acknowledge and enable bits
	TWCR &= ~( (1<<TWEA) | (1<<TWEN) );
}

ISR(TWI_vect){
	// Disable Global Interrupt
	//cli();

	// own address has been acknowledged by the master
	if((TWSR & 0xF8) == TW_SR_SLA_ACK) {
		// reset our current addr, data registers, and the state of TWI
		currentAddr = 0xFF;
		currentData = 0xFF;
		twiState = 0;
		bufferAddr_Proximities = 0;
		curIRDetector = 0;
		bufferAddr_Bearings = 0;
		
		// clear TWI interrupt flag, prepare to receive next byte and acknowledge
		TWCR |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
	}
	// data has been received in slave receiver mode
	else if((TWSR & 0xF8) == TW_SR_DATA_ACK) {
		/* check if reg addr or data */
		// first data byte, so it's address
		if (currentAddr == 0xFF) {
			currentAddr = TWDR;
			// clear TWI interrupt flag, prepare to receive next byte and acknowledge
			TWCR |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
		}
		// it's a data byte, reg addr has been already addressed
		else {
			// grab our data register value
			currentData = TWDR;
			
			/* check which reg addr was addressed */
			switch (currentAddr) {
				// RGB LED control register
				case RGBLED:
				// green LED
				if (currentData & 0b00000001)
				turnON_LEDG();
				else if (~currentData & 0b00000001)
				turnOFF_LEDG();
				// blue LED
				if (currentData & 0b00000010)
				turnON_LEDB();
				else if (~currentData & 0b00000010)
				turnOFF_LEDB();
				// red LED
				if (currentData & 0b00000100)
				turnON_LEDR();
				else if (~currentData & 0b00000100)
				turnOFF_LEDR();
				break;
				// control IR channel (frequency)
				case IRFREQ:
				if (twiState == 0) {
					pwmValue = (currentData << 8);
					twiState++;
				}
				else if (twiState == 1) {
					pwmValue |= currentData;
					set_IRFrequency(((unsigned short)pwmValue));
					twiState = 0;
				}
				break;
				case IRONOFF:
				// turn on the IR emitters
				if (currentData & 0b00000001) {
					turnON_PWM();
				}
				// turn off the IR emitters
				else if (~currentData & 0b00000001) {
					turnOFF_PWM();
				}
				break;
				case STARTP:
				startGettingProximities = (currentData & 0b00000001);
				break;
				case SETCHAN:
				set_IRFrequency(((unsigned short)channelFreq[currentData]));
				break;
				default:
				// do nothing...
				break;
			}
			
			// clear TWI interrupt flag, prepare to receive next byte and acknowledge
			TWCR |= (1<<TWIE) | (1<<TWINT) | (0<<TWEA) | (1<<TWEN);
		}
	}
	// 0xA8: SLA+R received, ACK returned
	// device has been addressed to be a transmitter
	else if(((TWSR & 0xF8) == TW_ST_SLA_ACK) || ((TWSR & 0xF8) == TW_ST_DATA_ACK)) {
		// transmit all the  measured proximities from all 6 IR phototransistors
		if (currentAddr == GETPROX) {
			// copy the specified buffer address into the TWDR register for transmission
			// send high 8 bits first
			if (bufferAddr_Proximities % 4 == 0) {
				TWDR = (uint8_t)((proximitiesIR[curIRDetector][(uint8_t)floor(bufferAddr_Proximities/4.0)] >> 24) & 0xFF);
			}
			else if (bufferAddr_Proximities % 4 == 1) {
				TWDR = (uint8_t)((proximitiesIR[curIRDetector][(uint8_t)floor(bufferAddr_Proximities/4.0)] >> 16) & 0xFF);
			}
			else if (bufferAddr_Proximities % 4 == 2) {
				TWDR = (uint8_t)((proximitiesIR[curIRDetector][(uint8_t)floor(bufferAddr_Proximities/4.0)] >> 8) & 0xFF);
			}
			// send low 8 bits
			else if (bufferAddr_Proximities % 4 == 3) {
				TWDR = (uint8_t)(proximitiesIR[curIRDetector][(uint8_t)floor(bufferAddr_Proximities/4.0)] & 0xFF);
			}
			// increment buffer read address
			bufferAddr_Proximities++;

			// check if we need to increment to next IR detector counter
			if (bufferAddr_Proximities >= (NUM_OF_CHANNELS * 4)) {
				curIRDetector++;
				bufferAddr_Proximities = 0;
			}
			// check if end of addr buffer
			if (curIRDetector >= NUM_OF_IRSENS) {
				// reset our addr buffer counter
				curIRDetector = 0;
				bufferAddr_Proximities = 0;
				// clear TWI interrupt flag, prepare to send last byte and receive not acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (0<<TWEA) | (1<<TWEN);
			}
			// if not, get ready to send next byte
			else {
				// clear TWI interrupt flag, prepare to send next byte and receive acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
			}
		}
		/*else if (currentAddr == GETSCAL) {
			// copy the specified buffer address into the TWDR register for transmission
			TWDR = scaledIR[curIRDetector][bufferAddr_Proximities];
			// increment buffer read address
			bufferAddr_Proximities++;
			
			// check if we need to increment to next IR detector counter
			if (bufferAddr_Proximities >= NUM_OF_CHANNELS) {
				curIRDetector++;
				bufferAddr_Proximities = 0;
			}
			// check if end of addr buffer
			if (curIRDetector >= 6) {
				// reset our addr buffer counter
				curIRDetector = 0;
				bufferAddr_Proximities = 0;
				// clear TWI interrupt flag, prepare to send last byte and receive not acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (0<<TWEA) | (1<<TWEN);
			}
			// if not, get ready to send next byte
			else {
				// clear TWI interrupt flag, prepare to send next byte and receive acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
			}
		}*/
		else if (currentAddr == GETDATA) {
			// check if end of addr buffer for bearings
			// start sending all the proximities
			// send our bearings for all channels
			if (bufferAddr_Bearings < (NUM_OF_CHANNELS * 2)) {
				// send high 8 bits
				if (bufferAddr_Bearings % 2 == 0) {
					TWDR = (uint8_t)((bearingsAllChan[(uint8_t)floor(bufferAddr_Bearings/2.0)] >> 8) & 0xFF);
				}
				// send low 8 bits
				else {
					TWDR = (uint8_t)(bearingsAllChan[(uint8_t)floor(bufferAddr_Bearings/2.0)] & 0xFF);
				}
				// increment buffer read address
				bufferAddr_Bearings++;
			}
			// send our proximities
			else if (bufferAddr_Bearings >= (NUM_OF_CHANNELS * 2)) {
				TWDR = proximitiesAllChan[bufferAddr_Proximities];
				bufferAddr_Proximities++;
			}
			// are we done with transmitting data or still more to go?
			// if done, send out the TWI command saying we has no more data
			if (bufferAddr_Proximities >= NUM_OF_CHANNELS) {
				// reset our addr buffer counter
				bufferAddr_Bearings = 0;
				bufferAddr_Proximities = 0;
				// clear TWI interrupt flag, prepare to send last byte and receive not acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (0<<TWEA) | (1<<TWEN);
			}
			// nope, we're not done so we will continue to send out data
			else {
				// clear TWI interrupt flag, prepare to send next byte and receive acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
			}
		}
		else if (currentAddr == GETOFFS) {
			// copy the specified buffer address into the TWDR register for transmission
			switch (bufferAddr_Proximities) {
				case 0:
				TWDR = (int8_t)(((int16_t)OFFSET_GYRO) & 0x00FF);
				break;
				case 1:
				TWDR = (int8_t)((((int16_t)OFFSET_GYRO) >> 8) & 0x00FF);
				break;
				case 2:
				TWDR = (int8_t)(((int16_t)OFFSET_ACCEL) & 0x00FF);
				break;
				case 3:
				TWDR = (int8_t)((((int16_t)OFFSET_ACCEL) >> 8) & 0x00FF);
				break;
				default:
				break;
			}
			// increment buffer read address
			bufferAddr_Proximities++;

			// check if end of addr buffer
			if (bufferAddr_Proximities >= 4) {
				// reset our addr buffer counter
				bufferAddr_Proximities = 0;
				// clear TWI interrupt flag, prepare to send last byte and receive not acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (0<<TWEA) | (1<<TWEN);
			}
			// if not, get ready to send next byte
			else {
				// clear TWI interrupt flag, prepare to send next byte and receive acknowledge
				TWCR |= (1<<TWIE) | (1<<TWINT) | (1<<TWEA) | (1<<TWEN);
			}
		}
	}
	else {
		// if none of the above apply prepare TWI to be addressed again
		TWCR |= (1<<TWIE) | (1<<TWEA) | (1<<TWEN);
	}
	
	// Enable Global Interrupt
	//sei();
}


