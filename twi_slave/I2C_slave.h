/*******************************************************************************************
*
* I2C_slave.h				Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/


#ifndef I2C_SLAVE_H
#define I2C_SLAVE_H


// RGB LED related macros
// FOR DEBUGGING PURPOSES!
#define	turnON_LEDG()	PORTD &= ~(1<<PD5)
#define	turnON_LEDB()	PORTB &= ~(1<<PB0)
#define	turnON_LEDR()	PORTB &= ~(1<<PB7)
#define	turnOFF_LEDG()	PORTD |= (1<<PD5)
#define	turnOFF_LEDB()	PORTB |= (1<<PB0)
#define	turnOFF_LEDR()	PORTB |= (1<<PB7)


#include <avr/io.h>
#include <util/twi.h>
#include <avr/interrupt.h>

#include "../ir_emitters/ir_emitters.h"
#include "../ir_detectors/ir_detectors.h"


/* reg hex values for TWI */
#define RGBLED	0x01
#define IRFREQ	0x02
#define IRONOFF	0x03
#define STARTP	0x04
#define GETPROX	0x05
#define SETCHAN	0x06
#define GETDATA	0x07
#define GETSCAL	0x08
#define GETOFFS	0x09


volatile uint8_t	currentAddr;
volatile uint8_t	currentData;

volatile uint8_t	curIRDetector;
volatile uint8_t	bufferAddr_Proximities;
volatile uint8_t	bufferAddr_Bearings;

volatile uint8_t	twiState;
volatile uint16_t	pwmValue;

volatile uint8_t	startGettingProximities;


void I2C_init(uint8_t address);
void I2C_stop(void);
ISR(TWI_vect);


#endif // I2C_SLAVE_H
