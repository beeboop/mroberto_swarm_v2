/*******************************************************************************************
*
* main.c					Copyright (C) 2016 Justin Y. Kim
*							last edit: April 5, 2016
*
*******************************************************************************************/


// RC calibration value in EEPROM, to be loaded to OSCCAL register at initialization
#define RC_CLK 0x0001

// RGB LED related macros
#define	turnON_LEDG()	PORTD &= ~(1<<PD5)
#define	turnON_LEDB()	PORTB &= ~(1<<PB0)
#define	turnON_LEDR()	PORTB &= ~(1<<PB7)
#define	turnOFF_LEDG()	PORTD |= (1<<PD5)
#define	turnOFF_LEDB()	PORTB |= (1<<PB0)
#define	turnOFF_LEDR()	PORTB |= (1<<PB7)

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include "usart/usart.h"
#include "twi_slave/I2C_slave.h"
#include "ir_emitters/ir_emitters.h"
#include "ir_detectors/ir_detectors.h"
#include "goertzel/goertzel.h"
#include "ir_detectors/ir_translator.h"


int main(void)
{
	/* read RC calibration value from EEPROM, and write it to OSCCAL register */
	//OSCCAL = 0xA2;
	//eeprom_write_byte ((uint8_t *)RC_CLK, 0xD9);
	OSCCAL = eeprom_read_byte((uint8_t *)RC_CLK);
	//while(1);
	
	//--- initialize our Goertzel frequencies/constants ---/
	// initialize our channel freq for ADC readings
	uint16_t channelFreqADC[NUM_OF_CHANNELS] = {CHANNEL_0_ADC, CHANNEL_1_ADC, CHANNEL_2_ADC,
	CHANNEL_3_ADC, CHANNEL_4_ADC};
	float coeffTest_FLOAT[NUM_OF_CHANNELS];
	initGoertzel(channelFreqADC, coeffTest_FLOAT);
	
	/** ENABLE RGB LEDS **/
	// change the RGB LED pins to output
	DDRD |= (1<<PD5);				// LED_G
	DDRB |= (1<<PB0) | (1<<PB7);	// LED_B and LED_R
	turnOFF_LEDG();
	turnOFF_LEDB();
	turnOFF_LEDR();
	
	/** ENABLE PWM & IR EMITTERS **/
	init_IREmitters();
	
	/** ENABLE ADC **/
	init_ADC();
	
	/** ENABLE TWI/I2C **/
	// initialize as TWI slave with address 0xA0
	I2C_init(0xA0);
	// allow interrupts for TWI
	sei();

	//--- ADC buffer ---/
	uint16_t	samplesADC[SAMPLE_SIZE];	// FHT_LEN is set to 256 samples

	// starts from FF position and go clockwise
	// i.e., ADC2 -> ADC3 -> ADC0 -> ADC6 -> ADC7 -> ADC1
	uint8_t		selectionADC[6] = {0x02, 0x03, 0x00, 0x06, 0x07, 0x01};
	float		scaledIR[NUM_OF_IRSENS][NUM_OF_CHANNELS];
	
	// GLOBAL PROXIMITIES IR VARIABLE SIGNIFICANTLY SLOWS DOWN PROCESS
	//uint16_t proximitiesIR[NUM_OF_IRSENS][NUM_OF_CHANNELS];
	int32_t multip, Q0, Q1, Q2;
	
	int16_t coeffTest[NUM_OF_CHANNELS];
	for (short i = 0; i < NUM_OF_CHANNELS; i++) {
		coeffTest[i] = (int32_t)(coeffTest_FLOAT[i] * (1 << COEFF_PRECISION));
	}


/*
setupUsart();
char output[64];
while(1) {
	// clear ADC channel mux register
	ADMUX &= 0xF0;
	// select which ADC channel to read from
	ADMUX |= 6;
	// get up to SAMPLE_SIZE number of samples
	for (short i = 0; i < SAMPLE_SIZE; i++) {
		// do single conversion
		ADCSRA |= (1<<ADSC);
		while(ADCSRA & (1<<ADSC));
		samplesADC[i] = (int16_t)ADCW;
	}
	//for (short i = 0; i < SAMPLE_SIZE; i++) {
	//	sprintf(output, "%d", samplesADC[i]);
	//	printf("%s, ", output);
	//}
	float freq;
	for (freq = 4500; freq <= 9500; freq += 50)
	{
		InitAndTest(freq, samplesADC);
	}
	//InitAndTest(5300, samplesADC);
	printf("\r\n\r\n");
	
	// wait for a char to be recieved
	while (!(UCSR0A & _BV(RXC0)));
	while (!(UCSR0A & _BV(UDRE0)));
	UDR0 = UDR0;
}*/


	

	//DDRB |= (1<<PB0);
	while(1) {
		//PORTB |= (1<<PB0);
		
		if (startGettingProximities) {
			for (uint8_t curIR = 0; curIR < NUM_OF_IRSENS; curIR++) {
				getAllIR(samplesADC, selectionADC[curIR]);
				// get the amplitudes of our selected channels (frequencies)
				for (uint8_t channelNum = 0; channelNum < NUM_OF_CHANNELS; channelNum++) {
					//--- process the samples ---/
					for (uint16_t index = 0; index < SAMPLE_SIZE; index++) {
						multip = coeffTest[channelNum] * Q1;
						Q0 = (multip >> COEFF_PRECISION) - Q2 + samplesADC[index];
						Q2 = Q1;
						Q1 = Q0;
					}
					//--- do the 'optimized' Goertzel processing ---/
					//uint16_t magnitude = (uint16_t)sqrt(getMagnitudeSquared(coeff[channelNum]));
					//uint16_t magnitude = (uint16_t)sqrt(Q1 * Q1 + Q2 * Q2 - Q1 * Q2 * coeffTest[channelNum]);
					// divide the answer by 2^6
					multip = coeffTest[channelNum] * Q1;
					proximitiesIR[curIR][channelNum] = ((uint16_t)sqrt(Q1 * Q1 + Q2 * (Q2 - (multip >> COEFF_PRECISION)))) >> MAG_REDUCE;
					// reset Goertzel
					Q1 = 0;
					Q2 = 0;
				}
			}
			
			// translate our raw IR readings to scaled IR readings
			translateRawIR(proximitiesIR, scaledIR);			
			// calculate the relative bearing for all channels
			getBearings(scaledIR);
			// grab the proximities for all channels
			getProximities(scaledIR, bearingsAllChan);
			
			// set our get proximity flag to 0
			startGettingProximities = 0;
		}
		
		//PORTB &= ~(1<<PB0);
		//_delay_ms(100);
	}

	return 0;
}

